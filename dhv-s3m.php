<?php
/*
	Plugin Name: DHV Streckmittelmeldungen Manager
	Plugin URI: http://hanfverband.de
	Description: DHV Streckmittelmeldungen verwalten und publizieren
	Version: 1.0.0
	Author: WeedCube Software
	Author URI: http://weed-cube.com
	License: GPL2
*/

require_once('lib/dhv-s3m-options.php');
require_once('lib/dhv-s3m-mysql.php');
require_once('lib/dhv-s3m-geocode.php');
require_once('lib/dhv-s3m-admin-bar.php');

if (is_admin()){
	require_once('lib/dhv-s3m-report-editor.php');
	require_once('lib/dhv-s3m-admin.php');
}else{
	require_once('lib/dhv-s3m-api.php');
	require_once('lib/dhv-s3m-user.php');
}

/* hook plugin activation, deactivation and uninstall */
if (is_admin()){
	register_activation_hook(__FILE__, array('DHV_S3M_Plugin', 'activate'));
	register_deactivation_hook(__FILE__, array('DHV_S3M_Plugin', 'deactivate'));
	register_uninstall_hook(__FILE__, array('DHV_S3M_Plugin', 'uninstall'));
}

/* handle plugin activation, deactivation and uninstall */
class DHV_S3M_Plugin
{
	public static function activate()
	{
		/* prepare plugin settings */
		global $dhvdb;
		global $dhv_s3m_option;
		$defopts = array(
			'ShowAdminAdvertisement' => TRUE
			);
		$opts = get_option($dhv_s3m_option);
		if (is_array($opts)) $opts = array_merge($defopts, $opts);
		else $opts = $defopts;
		update_option($dhv_s3m_option, $opts);
		$dhvdb->create_tables();
	}

	public static function deactivate()
	{
		/* nothing to be done */
	}

	public static function uninstall()
	{			
		global $dhvdb;
		global $dhv_s3m_option;
		delete_option($dhv_s3m_option);
		$dhvdb->drop_tables();
	}
};

?>