=== DHV-S3M ===
Contributors: weedcube
Donate link: http://weed-cube.com/
Tags: hanfverband, dhv, hanf, cannabis, streckmittel
Requires at least: 3.0.1
Tested up to: 3.4.2
Stable tag: 3.4.2
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Streckmittelmeldungen des Deutschen Hanf Verbandes verwalten und publizieren.

== Description ==

Dieser Plugin dient den Mitarbeitern des Deutschen Hanf Verbandes zum verwalten
einer Datenbank mit Warnungen vor gef�hrlichen Streckmitteln im Hanf (Cannabis).

Um Streckmittelwarnungen auf Ihrem Blog anzuzeigen nutzen sie bitte die "DHV-S2M"
Erweiterung.

== Installation ==

	1. Laden Sie den Plugin auf den gew�nschten Blog
	2. Aktivieren Sie den Plugin im Administrationsbereich im Men� "Plugins"

== Frequently Asked Questions ==

= Wie importiere ich vorhandene Streckmittelmeldungen? =

Im Administrationsbereich unter "Einstellungen => Streckmittelmeldungen"
kann im Abschnitt "Daten Import" eine ASCII Datei importiert werden.

= Wie frage ich die Datenbank ab? =

Vorgefertigte Abfragen k�nnen �ber einen feed des Blogs abgefragt werden. Die 
relative URL lautet "/?feed=dhv-s2m". Der nummerische Parameter "select" dient
zur Auswahl einer der folgenden Abfragen:

	[0] SELECT * FROM [TABLE] ORDER BY CUT_TS DESC 
	[1] SELECT * FROM [TABLE] ORDER BY CUT_TS DESC LIMIT 10

Beispiel "http://weed-cube.com/?feed=dhv-s2m&select=0"

(Die Variable [TABLE] wird automatisch entsprechend der Installation �bersetzt)

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets 
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png` 
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 1.0 =
* Initial Version

