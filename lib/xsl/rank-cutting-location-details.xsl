<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>  
  <table border="1">
	<tr bgcolor="#9acd32">		
		<th>Stadt</th>
		<th>Postleitzahl</th>
		<th>Streckmittel</th>
		<th>Meldungen</th>
		<th>Letzte Meldung</th>
		<th>Erste Meldung</th>
    </tr>
    <xsl:for-each select="STRECKMITTELMELDUNGEN/MELDUNG">
    <tr>
      <td><xsl:value-of select="CUT_CITY"/></td>      
      <td><xsl:value-of select="CUT_POSTCODE"/></td>
      <td><xsl:value-of select="CUT_NAME"/></td>      
      <td><xsl:value-of select="CUT_REPORTS"/></td>      
      <td><xsl:value-of select="CUT_LAST_SEEN"/></td>
      <td><xsl:value-of select="CUT_FIRST_SEEN"/></td>
    </tr>
    </xsl:for-each>
  </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet> 