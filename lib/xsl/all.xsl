<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>  
  <table border="1">
	<tr bgcolor="#9acd32">
		<th>Datum</th>
		<th>Stadt</th>
		<th>Postleitzahl</th>
		<th>Quelle</th>
		<th>Beschreibung</th>
		<th>Nebenwirkungen</th>
		<th>Nebenwirkungen</th>
		<th>Nebenwirkungen</th>
		<th>Bild URL</th>
    </tr>
    <xsl:for-each select="STRECKMITTELMELDUNGEN/MELDUNG">
    <tr>
      <td><xsl:value-of select="CUT_TS"/></td>      
      <td><xsl:value-of select="CUT_CITY"/></td>
      <td><xsl:value-of select="CUT_POSTCODE"/></td>
      <td><xsl:value-of select="CUT_ORIGIN"/></td>
      <td><xsl:value-of select="CUT_DESC"/></td>
      <td><xsl:value-of select="CUT_IMPAIR"/></td>
      <td><xsl:value-of select="CUT_IMPAIR_DESC"/></td>
      <td><xsl:value-of select="CUT_IMPAIR_DESC2"/></td>
      <td><xsl:value-of select="CUT_IMG_URL"/></td>      
    </tr>
    </xsl:for-each>
  </table>
  </body>
  </html>
</xsl:template>

</xsl:stylesheet> 