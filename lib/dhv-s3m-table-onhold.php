<?php

if(!class_exists('WP_List_Table')){
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class DHV_S3M_TableReportsOnHold extends WP_List_Table
{
	function __construct($args = array())
	{
		parent::__construct(array(
			'singular'  => 'location',     //singular name of the listed records
			'plural'    => 'locations',    //plural name of the listed records
			'ajax'      => false        //does this table support ajax?
		));
	}
	
	function prepare_items()
	{
		global $dhvdb;
		$per_page = 20;
		$cur_page = $this->get_pagenum();
		$skip = ($cur_page-1) * $per_page;
		$this->items = $dhvdb->get_reports_onhold($skip, $per_page);
		$total_items = count($this->items);
		$total_pages = ceil($total_items/$per_page);
		$this->_column_headers = array($this->get_columns(), array('CUT_AI'), array());
		$args = array(
			'per_page' => $per_page,
			'total_items' => $total_items,
			'total_pages' => $total_pages
		);
		$this->set_pagination_args($args);
	}

	function get_bulk_actions()
	{
		$actions = array(
			'unhold'    => 'Freischalten',
			'delete'    => 'L&ouml;schen'
		);
		return $actions;
	}

	function column_default($item, $column_name)
	{
		return $item[$column_name];
	}

	function column_CUT_CITY($item)
	{
		$actions = array(
			'unhold' =>  sprintf('<a href="?page=%s&action=%s&CUT_AI=%s">Freischalten</a>',$_REQUEST['page'], 'unhold', $item['CUT_AI']),
			'edit' =>  sprintf('<a href="?page=%s&action=%s&CUT_AI=%s">Bearbeiten</a>',$_REQUEST['page'], 'edit', $item['CUT_AI']),
			'delete' =>  sprintf('<a href="?page=%s&action=%s&CUT_AI=%s">L&ouml;schen</a>',$_REQUEST['page'], 'delete', $item['CUT_AI'])
			);
		return sprintf('%1$s %2$s', $item['CUT_CITY'], $this->row_actions($actions, FALSE));
	}

	function column_cb($item)
	{
		return sprintf('<input type="checkbox" name="CUT_AI[]" value="%s" />', $item['CUT_AI']);
	}

	function get_columns()
	{
		return array(
			'cb' => '<input type="checkbox" />',
			'CUT_AI' => 'ID',
			'CUT_CITY' => 'Stadt',
			'CUT_ORIGIN' => 'Herkunft',
			'CUT_DESC' => 'Beschreibung',
			'CUT_IMPAIR_DESC2' => 'Nebenwirkung',
			'CUT_IMG_URL' => 'Bildadresse'
		);
	}
}

?>