<?php

if(!function_exists('add_action')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

require_once('dhv-s3m-feeds.php');

class DHV_S3M
{
	public function __construct()
	{
		add_action('init', array($this, 'init'));
	}

	public function init()
	{
		add_feed('dhv-s2m', array($this, 'feed'));
		add_feed('dhv-s2m-api', array('DHV_S2M_API', 'print_page'));
	}

	public function feed()
	{
		global $dhvdb;
		global $dhv_s3m_feeds;

		$feed_id = isset($_GET['type']) ? $_GET['type'] : 0;
		$feed_id = is_int(0 + $feed_id) ? 0 + $feed_id : 0;

		$rows = $dhvdb->get_reports_feed($dhv_s3m_feeds[$feed_id]['query']);

		if ($rows){
			header("Content-Type: application/xml; charset=UTF-8");
			echo "<?xml version=\"1.0\" ?>\n";
			if (isset($dhv_s3m_feeds[$feed_id]['xsl'])){
				echo '<?xml-stylesheet type="text/xsl" href="' . $dhv_s3m_feeds[$feed_id]['xsl'] . '"?>';
			}
			echo "<STRECKMITTELMELDUNGEN>\n";
			echo " <META>";
			echo "  <NAME>" . $dhv_s3m_feeds[$feed_id]['name'] . "</NAME>";
			echo "  <DESCRIPTION>" . $dhv_s3m_feeds[$feed_id]['description'] . "</DESCRIPTION>";
			echo "  <TIMESTAMP>" . date('Y-m-d H:i:s') . "</TIMESTAMP>";
			echo " </META>";
			foreach ($rows as $row){
				echo " <MELDUNG>\n";
				foreach($row as $key => $value){
					echo  "<$key>" . dhv_s3m_xmlencode(html_entity_decode($value)) . "</$key>\n";
				}
				echo " </MELDUNG>\n";
			}
			echo "</STRECKMITTELMELDUNGEN>\n";
		}else{
			echo $dhvdb->last_error();
		}
	}
};

new DHV_S3M();

?>
