<?php

define('DHV_S3M_REPTABLE', $wpdb->prefix . 'dhv_s3m');
define('DHV_S3M_GEOTABLE', $wpdb->prefix . 'dhv_s3m_geo');

class DHV_S3M_Database
{
	private $reptable = DHV_S3M_REPTABLE;
	private $geotable = DHV_S3M_GEOTABLE;

	function __construct($args = array())
	{}

	function last_error()
	{
		global $wpdb;
		return $wpdb->last_error;
	}

	function get_humname($column)
	{
		$names = array(
				'CUT_TS' => 'Timestamp',
				'CUT_NAME' => 'Streckmittel',
				'CUT_CITY' => 'Stadt',
				'CUT_POSTCODE' => 'PLZ',
				'CUT_ORIGIN' => 'Herkunft',
				'CUT_DESC' => 'Beschreibung',
				'CUT_IMPAIR' => 'Konsumiert',
				'CUT_IMPAIR_DESC' => 'Nebenwirkung 1',
				'CUT_IMPAIR_DESC2' => 'Nebenwirkung 2',
				'CUT_IMG_URL' => 'Bildaddresse'
		);
		return isset($names[$column]) ? $names[$column] : $column;
	}
	
	function drop_tables()
	{
		global $wpdb;
		$wpdb->show_errors(FALSE);
		$wpdb->query("DROP TABLE IF EXISTS `$this->reptable`;");
		$wpdb->query("DROP TABLE IF EXISTS `$this->geotable`;");
		$wpdb->show_errors(TRUE);
		return $wpdb->last_error ? $wpdb->last_error : TRUE;
	}

	function create_tables()
	{
		/* update wordpress database structure */
		global $wpdb;
		/* create database table for reports */
		$wpdb->show_errors(FALSE);
		$sql = "CREATE TABLE IF NOT EXISTS `$this->reptable` (
		`CUT_AI` bigint unsigned NOT NULL AUTO_INCREMENT,
		`CUT_TS` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		`CUT_NAME` varchar(256) NOT NULL DEFAULT '',
		`CUT_CITY` varchar(64) NOT NULL DEFAULT '',
		`CUT_POSTCODE` varchar(16) NOT NULL DEFAULT '',
		`CUT_ORIGIN` varchar(256) NOT NULL DEFAULT '',
		`CUT_DESC` text NOT NULL DEFAULT '',
		`CUT_IMPAIR` tinyint(1) NOT NULL DEFAULT 0,
		`CUT_IMPAIR_DESC` text NOT NULL DEFAULT '',
		`CUT_IMPAIR_DESC2` text NOT NULL DEFAULT '',
		`CUT_IMG_URL` varchar(256) NOT NULL DEFAULT '',
		`CUT_HOLD_BACK` tinyint(1) NOT NULL DEFAULT 0,
		`CUT_REMOTE_SERVER` varchar(32) NOT NULL DEFAULT '',
		`CUT_REMOTE_CLIENT` varchar(32) NOT NULL DEFAULT '',
		PRIMARY KEY (`CUT_AI`),
		INDEX(`CUT_NAME`),
		INDEX(`CUT_CITY`),
		INDEX(`CUT_POSTCODE`)
		);";
		$wpdb->query($sql);
		/* create database table for geo locations */
		$sql = "CREATE TABLE IF NOT EXISTS `$this->geotable` (
		`GEO_AI` bigint unsigned NOT NULL AUTO_INCREMENT,
		`GEO_COUNTRY` varchar(32) NOT NULL,
		`GEO_STATE` varchar(32) NOT NULL,
		`GEO_CUT_CITY` varchar(64) NOT NULL,
		`GEO_LONG_CITY` varchar(64) NOT NULL,
		`GEO_SHORT_CITY` varchar(32) NOT NULL,
		`GEO_LNG` DECIMAL(10,7) NOT NULL,
		`GEO_LAT` DECIMAL(10,7) NOT NULL,
		`GEO_SW_LNG` DECIMAL(10,7) NOT NULL,
		`GEO_SW_LAT` DECIMAL(10,7) NOT NULL,
		`GEO_NE_LNG` DECIMAL(10,7) NOT NULL,
		`GEO_NE_LAT` DECIMAL(10,7) NOT NULL,
		PRIMARY KEY(`GEO_AI`),
		UNIQUE KEY(`GEO_CUT_CITY`),
		INDEX(`GEO_CUT_CITY`)
		);";
		$wpdb->query($sql);
		$wpdb->show_errors(TRUE);
		return $wpdb->last_error ? $wpdb->last_error : TRUE;
	}

	function count_reports()
	{
		global $wpdb;

		$wpdb->show_errors(FALSE);
		$count = $wpdb->get_var("SELECT COUNT(*) FROM `$this->reptable`;");
		$wpdb->show_errors(TRUE);

		return empty($wpdb->last_error) ? $count : -1;
	}

	function count_reports_onhold()
	{
		global $wpdb;

		$wpdb->show_errors(FALSE);
		$count = $wpdb->get_var("SELECT COUNT(*) FROM `$this->reptable` WHERE CUT_HOLD_BACK=1;");
		$wpdb->show_errors(TRUE);

		return $count ? $count : 0;
	}
	
	function get_reports_feed($sql)
	{
		global $wpdb;
		
		$wpdb->show_errors(FALSE);		
		$sql = str_replace('[TABLE]', "$this->reptable", $sql);
		$sql = str_replace('[GEOTABLE]', "$this->geotable", $sql);		
		$rows = $wpdb->get_results($sql, 'ARRAY_A');
		$wpdb->show_errors(TRUE);
		
		return $rows;
	}

	function get_report($ai)
	{
		global $wpdb;

		$wpdb->show_errors(FALSE);
		$data = $wpdb->get_row(
				$wpdb->prepare("SELECT * FROM `$this->reptable` WHERE CUT_AI=%d", $ai)
				,ARRAY_A
		);
		$wpdb->show_errors(TRUE);

		return $data;
	}

	function get_reports($skip=0, $max=NULL)
	{
		global $wpdb;

		$limit = '';
		$order = isset($_GET['order']) ? $_GET['order'] : "ASC";
		$orderby = isset($_GET['orderby']) ? $_GET['orderby'] : "CUT_TS";
		$orderby = "ORDER BY $orderby $order";
		if (isset($max)) $limit = "LIMIT $skip, $max";
		$wpdb->show_errors(FALSE);
		$data = $wpdb->get_results(
				"SELECT
				CUT_AI,
				CUT_TS,
				CUT_NAME,
				CUT_CITY,
				CUT_POSTCODE,
				CUT_ORIGIN,
				CUT_DESC,
				CUT_IMPAIR,
				CUT_IMPAIR_DESC,
				CUT_IMPAIR_DESC2,
				CUT_IMG_URL,
				CUT_HOLD_BACK,
				CUT_REMOTE_SERVER,
				CUT_REMOTE_CLIENT
				FROM `$this->reptable`
				$orderby
				$limit
				",
				ARRAY_A
		);
		$wpdb->show_errors(TRUE);

		return $data;
	}

	function get_reports_onhold($skip=0, $max=NULL)
	{
		global $wpdb;

		$limit = '';
		if (isset($max)) $limit = "LIMIT $skip, $max";
		$onhold = $wpdb->get_results(
				"SELECT
				CUT_AI,
				CUT_TS,
				CUT_NAME,
				CUT_CITY,
				CUT_POSTCODE,
				CUT_ORIGIN,
				CUT_DESC,
				CUT_IMPAIR,
				CUT_IMPAIR_DESC,
				CUT_IMPAIR_DESC2,
				CUT_IMG_URL,
				CUT_HOLD_BACK,
				CUT_REMOTE_SERVER,
				CUT_REMOTE_CLIENT
				FROM `$this->reptable`
				WHERE CUT_HOLD_BACK=1
				$limit
				",
				ARRAY_A
		);

		return $onhold;
	}

	/* return the number of currently unresolved reports */
	function count_unresolved_reports()
	{
		global $wpdb;
		
		$unresolved = $wpdb->get_var(
				"
				SELECT COUNT(*)
				FROM `$this->reptable`
				LEFT JOIN `$this->geotable` ON CUT_CITY=GEO_CUT_CITY
				WHERE GEO_CUT_CITY IS NULL
				;");

		return $unresolved;
	}

	/* get a list of unresolved reports from the database */
	function get_unresolved_reports($skip=0, $max=NULL)
	{
		global $wpdb;
		
		$wpdb->show_errors(FALSE);
		$unresolved = FALSE;
		if (isset($max)){
			$unresolved = $wpdb->get_results(
					"
					SELECT CUT_AI, CUT_POSTCODE, CUT_CITY, CUT_ORIGIN, CUT_TS
					FROM `$this->reptable`
					LEFT JOIN `$this->geotable` ON CUT_CITY=GEO_CUT_CITY
					WHERE GEO_CUT_CITY IS NULL
					ORDER BY CUT_CITY DESC
					LIMIT $skip,$max
					;", ARRAY_A);
		}else{
			$unresolved = $wpdb->get_results(
					"
					SELECT CUT_AI, CUT_POSTCODE, CUT_CITY, CUT_ORIGIN, CUT_TS
					FROM `$this->reptable`
					LEFT JOIN `$this->geotable` ON CUT_CITY=GEO_CUT_CITY
					WHERE GEO_CUT_CITY IS NULL
					ORDER BY CUT_CITY DESC
					;", ARRAY_A);
		}
		$wpdb->show_errors(TRUE);
		
		return $unresolved;
	}

	/* get a list of unresolved locations from the database */
	function get_unresolved_geo()
	{
		global $wpdb;
		
		$wpdb->show_errors(FALSE);
		$unresolved = $wpdb->get_col(
				"
				SELECT DISTINCT CUT_CITY FROM `$this->reptable`
				LEFT JOIN `$this->geotable` ON CUT_CITY=GEO_CUT_CITY
				WHERE GEO_CUT_CITY IS NULL
				;");
		$wpdb->show_errors(TRUE);
		
		return $unresolved;
	}

	/* insert report to database */
	function insert_rep($report)
	{
		global $wpdb;
		
		$wpdb->show_errors(FALSE);
		$wpdb->query($wpdb->prepare(
				"
				INSERT INTO `$this->reptable` (
				`CUT_NAME`,
				`CUT_CITY`,
				`CUT_POSTCODE`,
				`CUT_ORIGIN`,
				`CUT_DESC`,
				`CUT_IMPAIR`,
				`CUT_IMPAIR_DESC`,
				`CUT_IMPAIR_DESC2`,
				`CUT_IMG_URL`,
				`CUT_HOLD_BACK`,
				`CUT_REMOTE_SERVER`,
				`CUT_REMOTE_CLIENT`
		) VALUES(%s, %s, %s, %s, %s, 0, %s, %s, '', 1, %s, %s);
				",
				$report['cutting'],
				$report['city'],
				$report['postcode'],
				$report['origin'],
				$report['description'],
				isset($report['sideeffects']) ? $report['sideeffects'] : '',
				isset($report['sideeffects_description']) ? $report['sideeffects_description'] : '',
				$report['remote_server'],
				$report['remote_client']
		));
		
		return $wpdb->last_error ? $wpdb->last_error : TRUE;
	}

	function insert_import_rep($data, $overwrite=FALSE)
	{
		global $wpdb;

		$wpdb->show_errors(FALSE);
		if ($overwrite){
			$wpdb->query($wpdb->prepare(
					"
					INSERT INTO `$this->reptable` (
					`CUT_AI`,
					`CUT_TS`,
					`CUT_NAME`,
					`CUT_CITY`,
					`CUT_POSTCODE`,
					`CUT_ORIGIN`,
					`CUT_DESC`,
					`CUT_IMPAIR`,
					`CUT_IMPAIR_DESC`,
					`CUT_IMPAIR_DESC2`,
					`CUT_IMG_URL`,
					`CUT_HOLD_BACK`,
					`CUT_REMOTE_SERVER`,
					`CUT_REMOTE_CLIENT`
			) VALUES(%d, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 0, %s, %s);
					", $data
			));
		}else{
			$wpdb->query($wpdb->prepare(
					"
					INSERT INTO `$this->reptable` (
					`CUT_TS`,
					`CUT_NAME`,
					`CUT_CITY`,
					`CUT_POSTCODE`,
					`CUT_ORIGIN`,
					`CUT_DESC`,
					`CUT_IMPAIR`,
					`CUT_IMPAIR_DESC`,
					`CUT_IMPAIR_DESC2`,
					`CUT_IMG_URL`,
					`CUT_HOLD_BACK`,
					`CUT_REMOTE_SERVER`,
					`CUT_REMOTE_CLIENT`
			) VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, 0, %s, %s);
					", $data
			));
		}
		$wpdb->show_errors(TRUE);
		
		return $wpdb->last_error ? $wpdb->last_error : TRUE;
	}
	
	function update_rep($data, $where)
	{
		global $wpdb;
		
		$wpdb->show_errors(FALSE);
		$rval = $wpdb->update($this->reptable, $data, $where);
		$wpdb->show_errors(TRUE);
		
		return $rval !== FALSE ? TRUE : FALSE;
	}
	
	function update_rep_holdback($ai, $holdback)
	{
		global $wpdb;
		
		$wpdb->show_errors(FALSE);
		$rval = $wpdb->update($this->reptable, array('CUT_HOLD_BACK' => $holdback ? 1 : 0), array('CUT_AI' => $ai));
		$wpdb->show_errors(TRUE);
		
		return $rval !== FALSE ? TRUE : FALSE;
	}
	
	function update_city($currentName, $newName)
	{
		global $wpdb;
		
		$wpdb->show_errors(FALSE);
		$rval = $wpdb->update($this->reptable, array('CUT_CITY' => $newName), array('CUT_CITY' => $currentName));
		$wpdb->show_errors(TRUE);
		
		return $rval !== FALSE ? TRUE : FALSE;
	}

	function insert_geo($address, $encoded)
	{
		global $wpdb;

		$wpdb->show_errors(FALSE);
		$rval = $wpdb->query($wpdb->prepare(
				"
				INSERT IGNORE INTO `$this->geotable` (
				`GEO_COUNTRY`,
				`GEO_STATE`,
				`GEO_CUT_CITY`,
				`GEO_LONG_CITY`,
				`GEO_SHORT_CITY`,
				`GEO_LNG`,
				`GEO_LAT`,
				`GEO_SW_LNG`,
				`GEO_SW_LAT`,
				`GEO_NE_LNG`,
				`GEO_NE_LAT`
		) VALUES(%s, %s, %s, %s, %s, %f, %f, %f, %f, %f, %f)
				",
				array(
						htmlentities($encoded['Country']),
						htmlentities($encoded['State']),
						$address,
						htmlentities($encoded['LongName']),
						htmlentities($encoded['ShortName']),
						$encoded['Longitude'],
						$encoded['Latitude'],
						$encoded['SouthWestLongitude'],
						$encoded['SouthWestLatitude'],
						$encoded['NorthEastLongitude'],
						$encoded['NorthEastLatitude']
				)
		));
		$wpdb->show_errors(TRUE);
		
		return $wpdb->last_error ? $wpdb->last_error : TRUE;
	}

	function delete_rep($ai = NULL)
	{
		global $wpdb;
			
		$sql = "DELETE FROM `$this->reptable`";
		$sql .= $ai ? " WHERE CUT_AI=$ai;" : ";";
		$wpdb->show_errors(FALSE);
		$wpdb->query($sql);
		$wpdb->show_errors(TRUE);

		return $wpdb->last_error ? $wpdb->last_error : TRUE;
	}

	function delete_geo($ai = NULL)
	{
		global $wpdb;

		$sql = "DELETE FROM `$this->geotable`";
		$sql .= $ai ? " WHERE GEO_AI=$ai;" : ";";
		$wpdb->show_errors(FALSE);
		$wpdb->query($sql);
		$wpdb->show_errors(TRUE);

		return $wpdb->last_error ? $error = $wpdb->last_error : TRUE;
	}
};

$dhvdb = new DHV_S3M_Database();

?>