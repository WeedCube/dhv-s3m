<?php

if(!class_exists('WP_List_Table')){
	require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

class DHV_S3M_TableReports extends WP_List_Table
{
	function __construct($args = array())
	{
		parent::__construct(array(
			'singular'  => 'location',     //singular name of the listed records
			'plural'    => 'locations',    //plural name of the listed records
			'ajax'      => false        //does this table support ajax?
		));
	}

	function prepare_items()
	{
		global $dhvdb;
		$per_page = 20;
		$cur_page = $this->get_pagenum();
		$skip = ($cur_page-1) * $per_page;
		$this->items = $dhvdb->get_reports($skip, $per_page);
		$total_items = $dhvdb->count_reports();
		$total_pages = ceil($total_items/$per_page);
		$this->_column_headers = array(
			$this->get_columns(),
			array('CUT_AI'),
			$this->get_sortable_columns()
			);
		$args = array(
			'per_page' => $per_page,
			'total_items' => $total_items,
			'total_pages' => $total_pages
		);
		$this->set_pagination_args($args);
	}

	function get_bulk_actions()
	{
		$actions = array(
			'delete'    => 'L&ouml;schen'
		);
		return $actions;
	}

	function column_default($item, $column_name)
	{
		return $item[$column_name];
	}

	function column_CUT_CITY($item)
	{
		$actions = array(
			'edit' =>  sprintf('<a href="?page=%s&action=%s&CUT_AI=%s">Bearbeiten</a>',$_REQUEST['page'], 'edit', $item['CUT_AI']),
			'delete' =>  sprintf('<a href="?page=%s&action=%s&CUT_AI=%s">L&ouml;schen</a>',$_REQUEST['page'], 'delete', $item['CUT_AI'])
		);
		return sprintf('%1$s %2$s', $item['CUT_CITY'], $this->row_actions($actions, FALSE));
	}

	function column_cb($item)
	{			
		return sprintf('<input type="checkbox" name="CUT_AI[]" value="%s" />', $item['CUT_AI']);
	}

	function get_columns()
	{
		return array(
			'cb' => '<input type="checkbox" />',
			'CUT_AI' => 'ID',
			'CUT_TS' => 'Timestamp',
			'CUT_NAME' => 'Streckmittel',
			'CUT_POSTCODE' => 'PLZ',
			'CUT_CITY' => 'Stadt',
			'CUT_ORIGIN' => 'Herkunft',
			'CUT_DESC' => 'Beschreibung',
			'CUT_IMPAIR' => 'Konsumiert',
			'CUT_IMPAIR_DESC' => 'Nebenwirkungen',
			'CUT_IMPAIR_DESC2' => 'Nebenwirkung',
			'CUT_IMG_URL' => 'Bildadresse',
			'CUT_HOLD_BACK' => 'Zur&uuml;ckhalten',
			'CUT_REMOTE_SERVER' => 'S2M Server',
			'CUT_REMOTE_CLIENT' => 'S2M Client'
		);
	}

	function get_sortable_columns()
	{
		return array(
			'CUT_TS' => array('CUT_TS', true),
			'CUT_NAME' => array('CUT_NAME', false),
			'CUT_POSTCODE' => array('CUT_POSTCODE', false),
			'CUT_CITY' => array('CUT_CITY', false),
			'CUT_ORIGIN' => array('CUT_ORIGIN', false),
			'CUT_DESC' => array('CUT_DESC', false),
			'CUT_IMPAIR' => array('CUT_IMPAIR', false),
			'CUT_IMPAIR_DESC' => array('CUT_IMPAIR_DESC', false),
			'CUT_IMPAIR_DESC2' => array('CUT_IMPAIR_DESC2', false),
			'CUT_IMG_URL' => array('CUT_IMG_URL', false),
			'CUT_HOLD_BACK' => array('CUT_HOLD_BACK', false),
			'CUT_REMOTE_SERVER' => array('CUT_REMOTE_SERVER', false),
			'CUT_REMOTE_CLIENT' => array('CUT_REMOTE_CLIENT', false)
		);
	}
}

?>