<?php

if(!function_exists('add_action')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

global $dhv_s3m_feeds;

$dhv_s3m_feeds = array(
		array(
				'name' => 'all',
				'description' => "Alle Meldungen absteigend nach Datum sortiert",
				'query' =>
				"
				SELECT
				CUT_TS,
				CUT_NAME,
				CUT_CITY,
				CUT_POSTCODE,
				CUT_ORIGIN,
				CUT_DESC,
				CUT_IMPAIR,
				CUT_IMPAIR_DESC,
				CUT_IMPAIR_DESC2,
				CUT_IMG_URL
				FROM [TABLE]
				WHERE
					CUT_HOLD_BACK=0
				ORDER BY
					CUT_TS DESC;
				",
				'xsl' => plugins_url('xsl/all.xsl', __FILE__)
		),
		array(
				'name' => 'rank-cutting-location',
				'description' => "Alle Meldungen nach Ort und Streckmittel gruppiert und nach Anzahl und Ort der Meldungen sortiert",
				'query' =>
				"
				SELECT
				CUT_CITY,
				CUT_POSTCODE,
				COUNT(CUT_CITY) AS CUT_REPORTS,
				MAX(CUT_TS) AS CUT_LAST_SEEN,
				MIN(CUT_TS) AS CUT_FIRST_SEEN
				FROM [TABLE]
				WHERE
					CUT_HOLD_BACK=0
				GROUP BY
					CUT_CITY
				ORDER BY
					CUT_REPORTS DESC,
					CUT_CITY ASC,
					CUT_CITY ASC;
				",
				'xsl' => plugins_url('xsl/rank-cutting-location.xsl', __FILE__)
		),
		array(
				'name' => 'rank-cutting-location-details',
				'description' => "Alle Meldungen nach Ort und Streckmittel gruppiert und nach Ort und Anzahl der Meldungen sortiert",
				'query' =>
				"
				SELECT
				CUT_CITY,
				CUT_POSTCODE,
				CUT_NAME,
				COUNT(CUT_NAME) AS CUT_REPORTS,
				MAX(CUT_TS) AS CUT_LAST_SEEN,
				MIN(CUT_TS) AS CUT_FIRST_SEEN
				FROM [TABLE]
				WHERE
					CUT_HOLD_BACK=0
				GROUP BY
					CUT_CITY,
						CUT_NAME
				ORDER BY
					CUT_CITY ASC,
					CUT_REPORTS DESC,
					CUT_CITY ASC;
				",
				'xsl' => plugins_url('xsl/rank-cutting-location-details.xsl', __FILE__)
		),
		array(
				'name' => 'rank-by-timestamp',
				'description' => "",
				'query' =>
				"
				SELECT
					COUNT(*) AS CUT_COUNT,
					MAX(CUT_TS) AS TIMESTAMP,
					GEO_LAT,
					GEO_LNG
				FROM [TABLE] INNER JOIN [GEOTABLE]
				ON
					CUT_CITY=GEO_CUT_CITY
					AND CUT_HOLD_BACK=0
				GROUP BY
					CUT_CITY
				ORDER BY
					TIMESTAMP DESC
				"
		)
);

function dhv_s3m_xmlencode($string)
{
	return str_replace(
			array("&", "<", ">", "\"", "'"),
			array("&amp;", "&lt;", "&gt;", "&quot;", "&apos;"),
			$string
	);
}

?>
