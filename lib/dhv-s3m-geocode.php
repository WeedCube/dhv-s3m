<?php

if(!function_exists('add_action')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

class DHV_S3M_Geocode
{
	/* This methods tries to geocode the given $address using google's map api
	 * and returns the gathered information as array. If anything went wrong
	* the array member 'Error' is set appropriately
	*/
	public static function geocode($address)
	{
		set_time_limit(60);
		/* setup default data */
		$data = array(
			'Error' => NULL,
			'Status' => '',
			'Country' => 'Unbekannt',
			'State' => 'Unbekannt',
			'LongName' => 'Unbekannt',
			'ShortName' => 'Unbekannt',
			'Country' => 'Unbekannt',
			'Longitude' => '',
			'Latitude' => '',
			'SouthWestLongitude' => '',
			'SouthWestLatitude' => '',
			'NorthEastLongitude' => '',
			'NorthEastLatitude' => '',
		);
		/* setup google maps api request */
		$encoded_address = urlencode(html_entity_decode($address, ENT_COMPAT, 'UTF-8'));
		$region = 'de';
		$lang = 'de';
		$url = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&region=$region&language=$lang&address=$encoded_address";
		$json = @file_get_contents($url);
		$json = json_decode($json, TRUE);
		$delimiter = array(',', '-', '/', ' ');
		while ((!isset($json['results']) || count($json['results'])) == 0 && count($delimiter) > 0){
			/* Heuristic approach
			 * 1. split address by commonly used delimiters
			* 2. ...
			* Todo:
			* - postcode based approach
			* - maybe add google suggest
			* - maybe add spellchecking
			*/
			$d = array_shift($delimiter);
			if (!strstr($address, $d)) continue;
			$candidates = explode($d, $address);
			foreach ($candidates as $candidate){
				$encoded_address = urlencode(utf8_encode($candidate));
				$url = "http://maps.googleapis.com/maps/api/geocode/json?sensor=false&region=$region&language=$lang&address=$encoded_address";
				$json = file_get_contents($url);
				$json = json_decode($json, TRUE);
			}
		}
		/* parse results returned by google map api and be return */
		foreach ($json['results'] as $result){
			foreach ($result['address_components'] as $address_component){
				if (in_array('locality', $address_component['types'])){
					$data['LongName'] = $address_component['long_name'];
					$data['ShortName'] = $address_component['short_name'];
				}
				if (in_array('country', $address_component['types']))
					$data['Country'] = $address_component['long_name'];

				if (in_array('administrative_area_level_1', $address_component['types']))
					$data['State'] = $address_component['long_name'];
			}
			if (isset($result['geometry']['location'])){
				$data['Latitude'] = $result['geometry']['location']['lat'];
				$data['Longitude'] = $result['geometry']['location']['lng'];
			}
			if (isset($result['geometry']['bounds'])){
				$data['NorthEastLatitude'] = $result['geometry']['bounds']['northeast']['lat'];
				$data['NorthEastLongitude'] = $result['geometry']['bounds']['northeast']['lng'];
				$data['SouthWestLatitude'] = $result['geometry']['bounds']['southwest']['lat'];
				$data['SouthWestLongitude'] = $result['geometry']['bounds']['southwest']['lng'];
			}
		}
		/* check error conditions and setup return values accordingly */
		if (!isset($json['results'])){
			$data['Error'] = 'failed to query Google Maps API for ' . "\"$address\"";
			$data['State'] = 'CONNECTION_ERROR';
		}
		if (count($json['results']) == 0){
			$data['Error'] = 'Google Maps API did not return any results for ' . "\"$address\"";
			$data['Status'] = $json['status'];
		}
		return $data;
	}
};

?>