<?php

if(!function_exists('add_action')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

class DHV_S3M_AdminBar
{
	public function __construct()
	{
		add_action('wp_before_admin_bar_render', array($this, 'init_admin_bar'));
	}

	public function init_admin_bar()
	{
		if (!current_user_can( 'moderate_comments' ))
			return;

		global $dhvdb;
		global $wp_admin_bar;

		$tasks_geocode = count($dhvdb->get_unresolved_geo());
		$tasks_reports = $dhvdb->count_reports_onhold();
		$total_tasks = $tasks_geocode + $tasks_reports;

		$title = '<img style="margin-top: 5px" src="'.plugins_url().'/dhv-s3m/images/leaf_16x16.png" />';
		$title_unhold = 'Streckmittelmeldungen';
		$title_geocode = 'Geokodierung';

		if ($total_tasks > 0) $title .= " $total_tasks";
		if ($tasks_reports > 0) $title_unhold .= " ($tasks_reports)";
		if ($tasks_geocode > 0) $title_geocode .= " ($tasks_geocode)";

		$wp_admin_bar->add_node(array(
			'id' => 'dhv-s3m-admin-bar-admin',
			'title' => $title,
			'href' => admin_url('/admin.php?page=dhv-s3m-admin')
		));
		$wp_admin_bar->add_node(array(
			'id' => 'dhv-s3m-admin-bar-unhold-report',
			'title' => $title_unhold,
			'parent' => 'dhv-s3m-admin-bar-admin',
			'href' => admin_url('/admin.php?page=dhv-s3m-unhold-report')
		));
		$wp_admin_bar->add_node(array(
			'id' => 'dhv-s3m-admin-bar-geocode',
			'title' => $title_geocode,
			'parent' => 'dhv-s3m-admin-bar-admin',
			'href' => admin_url('/admin.php?page=dhv-s3m-geocode')
		));
	}
};

new DHV_S3M_AdminBar();

?>