<?php

if(!function_exists('add_action')) {
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

class DHV_S2M_API
{
	public static function print_page()
	{
		header("Content-Type: application/xml; charset=UTF-8");
		echo DHV_S2M_API::create_page();
	}

	public static function create_page()
	{
		if (isset($_GET['dhv-s2m-api'])){
			$action = $_GET['dhv-s2m-api']['action'];
			switch ($action)
			{
				case 'receive-report':
					return DHV_S2M_API::create_receive_report($_GET['dhv-s2m-api']['params']);

				default:
					return DHV_S2M_API::create_status_reply('Unknown action', 3);
			}
		}
		return DHV_S2M_API::create_status_reply('Syntax error', 2);
	}

	private static function create_status_reply($message = "Unknown Error", $code = 1)
	{
		$xml = '<?xml version="1.0" ?>' . "\n";
		$xml .= '<DHV-S2M-API>' . "\n";
		$xml .= ' <STATUS>' . "\n";
		$xml .= '  <CODE>'.$code.'</CODE>' . "\n";
		$xml .= '  <MESSAGE>'.$message.'</MESSAGE>' . "\n";
		$xml .= ' </STATUS>' . "\n";
		$xml .= '</DHV-S2M-API>';
		
		return $xml;
	}

	private static function create_receive_report($params)
	{		
		global $dhvdb;		
		
		$params = str_replace('|', '&', $params);
		$params = str_replace(':', '=', $params);
		parse_str($params, $opts);
		/* sanitize input */
		foreach($opts as $opt => $value){
			$opts[$opt] = htmlentities(wp_kses_data(trim($value)));
		}
		/* reply error on missing arguments */
		$mandantory_opts = array(
			'cutting', 'city', 'postcode', 'origin', 'description',
			'application', 'remote_server', 'remote_client'
			);
		foreach ($mandantory_opts as $opt){
			if (!isset($opts[$opt]) || empty($opts[$opt]))
				return DHV_S2M_API::create_status_reply("Missing parameter: [$opt]", 4);
		}
		/* try to geocode and update database */				
		$encoded = DHV_S3M_Geocode::geocode($opts['city']);
		if (!isset($encoded['Error'])) $dhvdb->insert_geo($opts['city'], $encoded);	
		$dhvdb->insert_rep($opts);
		
		return DHV_S2M_API::create_status_reply('OK', 0);
	}
};

?>