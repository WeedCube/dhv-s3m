<?php 

class DHV_S3M_ReportEditor
{
	public function __construct($args = array())
	{}

	public function display()
	{
		global $dhvdb;

		$fields = array(
				'CUT_TS' => 'text',
				'CUT_NAME' => 'text',
				'CUT_CITY' => 'text',
				'CUT_POSTCODE' => 'text',
				'CUT_ORIGIN' => 'textarea',
				'CUT_DESC' => 'textarea',
				'CUT_IMPAIR' => 'text',
				'CUT_IMPAIR_DESC' => 'textarea',
				'CUT_IMPAIR_DESC2' => 'textarea',
				'CUT_IMG_URL' => 'text'
		);

		if (isset($_POST['dhv-s3m-edit-report-save'])){
			$data = array();
			foreach($fields as $column => $type){
				if (!isset($_POST['dhv_s3m_edit_save_'.$column])){
					$errors[] = 'dhv_s3m_edit_save_' . $column . ' wurde nicht &uuml;bergeben!';
					$data = array();
					break;
				}
				$data[$column] = $_POST['dhv_s3m_edit_save_'.$column];
			}
			if (count($data) == count($fields)){
				if ($dhvdb->update_rep($data, array('CUT_AI' => $_GET['CUT_AI'])) === TRUE) $infos[] = 'Report erfolgreich aktualisiert!';
				else $errors[] = 'Report konnte nicht gespeichert werden!';
			}else{
				$errors[] = 'Report konnte nicht gespeichert werden!';
			}
		}else{
			if (isset($_GET['CUT_AI'])){
				$rval = $dhvdb->get_report($_GET['CUT_AI']);
				if ($dhvdb->last_error()){
					$errors[] = 'Fehler beim auslesen der Datenbank!';
				}else{
					echo '<div id="dhv_s3m_edit_report">';
					echo '<h3>Streckmittelmeldung bearbeiten</h3>';
					echo '<form method="POST">';
					foreach ($fields as $column => $type){
						echo '<p>'.$dhvdb->get_humname($column).':</p>';
						switch ($type){
							case 'text':
								printf('<input style="width:60%%" type="%s" value="%s" name="dhv_s3m_edit_save_%s" />', $type, $rval[$column], $column);
								break;

							case 'textarea':
								printf('<textarea style="height:120px; width:60%%" name="dhv_s3m_edit_save_%s">%s</textarea>', $column, $rval[$column]);
								break;
						}
					}
					echo
					'
							<p>
							<input type="submit" name="dhv-s3m-edit-report-save" value="Speichern" />
							</p>
							</form>
							';
					echo '</div>';
				}
			}
		}
	}

};

?>