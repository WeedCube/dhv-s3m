<?php

if (!function_exists('add_action')){
	header('Status: 403 Forbidden');
	header('HTTP/1.1 403 Forbidden');
	exit();
}

if(!class_exists('DHV_S3M_TableGeocode')){
	require_once('dhv-s3m-table-geocode.php');
}

if(!class_exists('DHV_S3M_TableReportsOnHold')){
	require_once('dhv-s3m-table-onhold.php');
}

if(!class_exists('DHV_S3M_TableReports')){
	require_once('dhv-s3m-table-reports.php');
}

class DHV_S3M_Admin
{
	public function __construct()
	{
		add_action('admin_init', array($this, 'admin_init'));
		add_action('admin_menu', array($this, 'admin_menu'));
	}

	public function create_message_box($message, $error = FALSE)
	{
		if ($error) return '<div class="error"><p>'.$message.'</p></div>';
		return '<div class="updated"><p>'.$message.'</p></div>';
	}

	public function admin_init()
	{
		global $dhv_s3m_option;
		$conf = get_option($dhv_s3m_option);

		add_settings_section('dhv-s3m-settings-section', 'Allgemein', array($this, 'settings_section'), 'dhv-s3m-settings-page');

		add_settings_field(
		$dhv_s3m_option .'[ShowAdminAdvertisement]',
		'Werbung im Admin Bereich; anzeigen',
		array($this, 'create_checkbox'),
		'dhv-s3m-settings-page',
		'dhv-s3m-settings-section',
		array($dhv_s3m_option . '[ShowAdminAdvertisement]', $conf['ShowAdminAdvertisement'])
		);

		register_setting('dhv-s3m-settings-group', $dhv_s3m_option, array($this, 'admin_validate'));
	}

	public function admin_validate($input)
	{
		global $dhv_s3m_option;
		$conf = get_option($dhv_s3m_option);
		$conf['ShowAdminAdvertisement'] = isset($input['ShowAdminAdvertisement']);
		return $conf;
	}

	public function create_checkbox($arg)
	{
		if ($arg[1] == TRUE) echo '<input type="checkbox" name="'.$arg[0].'" value="'.$arg[1].'" checked />';
		else echo '<input type="checkbox" name="'.$arg[0].'" value="'.$arg[1].'" />';
	}

	public function admin_menu()
	{
		add_menu_page(
		'Hanfverband',
		'Hanfverband',
		'moderate_comments',
		'dhv-s3m-admin',
		array($this, 'admin_page'),
		plugins_url() . '/dhv-s3m/images/dhv_24x24.png'
				);

		add_submenu_page(
		'dhv-s3m-admin',
		'Streckmittelmeldungen',
		'Streckmittelmeldungen',
		'moderate_comments',
		'dhv-s3m-unhold-report',
		array($this, 'unhold_page')
		);

		add_submenu_page(
		'dhv-s3m-admin',
		'Geokodierung',
		'Geokodierung',
		'moderate_comments',
		'dhv-s3m-geocode',
		array($this, 'geocode_page')
		);

		add_submenu_page(
		'dhv-s3m-admin',
		'Daten Import',
		'Daten Import',
		'moderate_comments',
		'dhv-s3m-import',
		array($this, 'dataimport_page')
		);

		add_submenu_page(
		'dhv-s3m-admin',
		'Einstellungen',
		'Einstellungen',
		'manage_options',
		'dhv-s3m-settings-page',
		array($this, 'settings_page')
		);
	}

	public function common_header($title, $subtitle = null)
	{
		if (!isset($subtitle))
			$subtitle = 'For more information about this plugin please visit <a href="http://weed-cube.com">http://weed-cube.com</a>';
		echo
		'
				<div class="wrap">
				<div class="icon32" id="icon-options-general">
				<br />
				</div>
				<h2>'.$title.'</h2>
						'.$subtitle.'
								';

		global $dhv_s3m_option;
		$conf = get_option($dhv_s3m_option);
		if ($conf['ShowAdminAdvertisement']){
			echo
			'
					<h3>Advertisement</h3>
					<script type="text/javascript"><!--
					google_ad_client = "ca-pub-7930794124444982";
					/* WeedCube_DHV_Streckmittelmeldungen */
					google_ad_slot = "8647057292";
					google_ad_width = 728;
					google_ad_height = 90;
					//-->
					</script>
					<script type="text/javascript"
					src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
					</script>
					<hr />
					';
		}
	}

	public function common_footer()
	{
		echo '</div>';
	}

	public function admin_page()
	{
		global $dhvdb;
		$errors = array();
		$infos = array();

		$this->common_header('Hanfverband');

		if (isset($_GET['action'])){
			$action = $_GET['action'];
			switch ($action)
			{
				case 'delete':
					if (isset($_GET['CUT_AI'])){
						$ais = $_GET['CUT_AI'];
						if (!is_array($ais)) $ais = array($ais);
						foreach($ais as $ai){
							if ($dhvdb->delete_rep($ai) !== TRUE) $errors[] = "<p>".$dhvdb->last_error()."</p>";
							else $infos[] = "<p>Datensatz $ai wurde gel&ouml;scht.</p>";
						}
					}else $errors[] = "<p>Es wurden keine Daten ausgew&auml;hlt!</p>";
					break;

				case 'edit':
					$editor = new DHV_S3M_ReportEditor();
					$editor->display();
					break;

				default:
					break;
			}
			if (!empty($errors)) echo $this->create_message_box(implode('<br/>', $errors), TRUE);
			if (!empty($infos)) echo $this->create_message_box(implode('<br/>', $infos));
		}

		$table = new DHV_S3M_TableReports();
		$table->prepare_items();
		echo '<h3>Alle Streckmittelmeldungen</h3>';
		echo '<form method="GET">';
		echo '<input type="hidden" name="page" value="'.$_REQUEST['page'].'" />';
		echo $table->display();
		echo '</forum>';

		$this->common_footer();
	}

	public function unhold_page()
	{
		global $dhvdb;
		
		$this->common_header('Streckmittelmeldungen publizieren');		

		if (isset($_GET['CUT_AI']) && isset($_GET['action'])){
			$action = $_GET['action'];
			$cut_ai = is_array($_GET['CUT_AI']) ? $_GET['CUT_AI'] : array($_GET['CUT_AI']);
			foreach($cut_ai as $ai){
				switch($action)
				{
					case 'unhold':						
						if ($dhvdb->update_rep_holdback($ai, FALSE) !== TRUE)
							echo $this->create_message_box($dhvdb->last_error, TRUE);
						else
							echo $this->create_message_box('Datensatz Nr.'.$ai.' wurde erfolgreich freigegeben.', FALSE);
							
						break;
						
					case 'edit':
						$editor = new DHV_S3M_ReportEditor();
						$editor->display();
						break;

					case 'delete':
						if ($dhvdb->delete_rep($ai) !== TRUE)
							echo $this->create_message_box("Datensatz Nr.".$ai." konnte nicht gel&ouml;scht werden: ".$dhvdb->last_error(), TRUE);
						else
							echo $this->create_message_box("Datensatz Nr.".$ai." wurde gel&ouml;scht.", FALSE);
						break;
				}
			}
		}
		echo '<form method="GET">';
		echo '<input type="hidden" name="page" value="'.$_REQUEST['page'].'" />';
		$table = new DHV_S3M_TableReportsOnHold();
		$table->prepare_items();
		echo '<h3>Streckmittelmeldungen freischalten oder l&ouml;schen</h3>';
		if ($table->has_items()) echo '<p>Die folgenden Beitr&auml;ge m&uuml;ssen gepr&uuml;ft und ggf. freigeschaltet werden:</p>';
		else echo "<p>Derzeit liegen keine neuen Streckmittelmeldungen vor.</p>";
		echo $table->display();
		echo '</form>';

		$this->common_footer();
	}

	public function geocode_page()
	{
		global $dhvdb;
		$info = NULL;
		$error = NULL;
		$count_geocoded=0;

		$this->common_header('Geokodierung durchf&uuml;hren');

		$unresolved = $dhvdb->get_unresolved_geo();
		if ($dhvdb->last_error()) $error .= "<p>Geokodierung fehlgeschlagen: $dhvdb->last_error()</p>";

		if (isset($_POST['dhv-s3m-csv-geocode-submit'])){
			foreach ($unresolved as $address){				
				$encoded = DHV_S3M_Geocode::geocode($address);
				if (isset($encoded['Error']))  $error .= "<p>Geokodierung fehlgeschlagen: $encoded[Status]</p>";
				else if ($dhvdb->insert_geo($address, $encoded) !== TRUE) $error .= "<p>$dhvdb->last_error()</p>";
				else $count_geocoded++;
			}
			$info .= "<p>$count_geocoded Datens&auml;tze wurden geokodiert.</p>";
		}else if (isset($_POST['dhv-s3m-csv-geocode-delete-db'])){
			$dhvdb->delete_geo();
			$info = '<p>Datenbank wurde erfolgreich geleert</p>';
		}else if (isset($_POST['dhv-s3m-edit-city-save'])){			
			$currentName = isset($_GET['CUT_CITY']) ? $_GET['CUT_CITY'] : "";			
			$newName = isset($_POST['dhv_s3m_edit_save_city']) ? $_POST['dhv_s3m_edit_save_city'] : ""; 			
			if (empty($newName)){
				$error .= '<p>Die Stadt darf nicht leer sein!</p>';
			}else if ($dhvdb->update_city($currentName, $newName)){							
				$encoded = DHV_S3M_Geocode::geocode($newName);
				if (isset($encoded['Error'])) $error .= "<p>Geokodierung fehlgeschlagen: $encoded[Status]</p>";
				else if ($dhvdb->insert_geo($newName, $encoded) !== TRUE) $error .= "<p>$dhvdb->last_error()</p>";
				else $info .= '<p>Stadt wurde ge&auml;ndert.</p>';				
			}else{
				$error .= '<p>Fehler beim &auml;ndern der Stadt: '.$dhvdb->last_error().'!</p>';
			}
			
		}else if (isset($_GET['action']) && isset($_GET['CUT_CITY'])){
			$action = $_GET['action'];
			$city = $_GET['CUT_CITY'];

			switch ($action)
			{
				case 'edit':
					echo
					'
							<div id="dhv_s3m_edit_report">
							<h3>Stadt bearbeiten</h3>
							<form method="POST">
							<input style="width:60%" type="text" name="dhv_s3m_edit_save_city" value="'.$city.'" />
							<input type="submit" name="dhv-s3m-edit-city-save" value="Speichern" />
							</form>
							</div>
							';
					break;

				default:
					break;
			}
		}

		if ($error) echo $this->create_message_box($error, TRUE);
		if ($info) echo $this->create_message_box($info, FALSE);

		printf(
		'
				<h3>Geokodierung</h3>
				<form method="post">
				<p>Geokodierung muss f&uuml;r %d St&auml;dte durchgef&uuml;hrt werden</p>
				', count($unresolved)-$count_geocoded);
		echo
		'
				<input class="button-primary" type="submit" name="dhv-s3m-csv-geocode-submit" value="Geokodierung durchf&uuml;hren"></input>
				<input class="button-primary" type="submit" name="dhv-s3m-csv-geocode-delete-db" value="Geokodierung vollst&auml;nding l&ouml;schen"></input>
				</form>
				';
		$test = new DHV_S3M_TableGeocode();
		$test->prepare_items();
		echo $test->display();
		$this->common_footer();
	}

	public function dataimport_page()
	{
		global $dhvdb;
		$info = FALSE;
		$error = FALSE;

		$this->common_header('Daten importieren');

		if (!$error && (isset($_POST['dhv-s3m-csv-import-delete-db']) || isset($_POST['dhv-s3m-csv-import-overwrite']))){
			if ($dhvdb->delete_rep() !== TRUE) $error = $dhvdb->last_error();
			else $info = '<p>Datenbank wurde erfolgreich geleert</p>';
		}

		if (!$error && isset($_POST['dhv-s3m-csv-import-submit'])){
			if ($_FILES['dhv-s3m-filename']['error'] > 0 || empty($_FILES['dhv-s3m-filename']['tmp_name'])){
				$error = 'Fehler beim hochladen der Streckmittel Textdatei: ' . $_FILES['dhv-s3m-filename']['error'];
			}else{
				$filename = $_FILES['dhv-s3m-filename']['tmp_name'];
				if(($handle = @fopen($filename, "r")) === FALSE){
					$error = '';
				}else{
					/* process csv upload */
					if ($handle !== FALSE){
						$count_imported=0;
						$count_geocoded=0;
						while (($data = fgetcsv($handle, 8192, "|")) !== FALSE){
							set_time_limit(90);
							for ($i=0; $i<11; $i++){
								if (!isset($data[$i])) $data[$i] = '';
								else $data[$i] = htmlentities(wp_kses_post(trim($data[$i])), ENT_COMPAT, 'ISO-8859-15');
							}
							if (($data[1] = date('Y-m-d H:i:s', $data[1])) == FALSE){
								$data[1] = date('Y-m-d H:i:s', time());
							};
							$data[7] = strtolower($data[7]) == "ja" ? '1' : '0';
							if (strlen($data[4]) == 2) $data[4] = 'de-' . $data[4];
							$data[4] = strtoupper($data[4]);
							$data[] = $_SERVER['SERVER_NAME'];
							$data[] = $_SERVER['REMOTE_ADDR'];
							if (isset($_POST['dhv-s3m-csv-import-geocode'])){
								$encoded = DHV_S3M_Geocode::geocode($data[3]);
								if (isset($encoded['Error'])) $error .= "$encoded[Status]<br/>";
								else if ($dhvdb->insert_geo($data[3], $encoded) !== TRUE) $error .= "$dhvdb->last_error()<br/>";
								else $count_geocoded++;
							}
							if (isset($_POST['dhv-s3m-csv-import-overwrite'])){
								$dhvdb->insert_import_rep($data, TRUE);
							}else{
								array_shift($data);
								$dhvdb->insert_import_rep($data, FALSE);
							}
							if ($dhvdb->last_error()) $error .= '<p><b>Datensatz:</b><br/>' . implode('|', $data) . "<br/><br/><b>Fehler:</b><br/><i>$dhvdb->last_error()</i><p>";
							else $count_imported++;
						}
						$info .= "<p>$count_geocoded Datens&auml;tze wurden geokodiert.</p>";
						$info .= "<p>$count_imported Datens&auml;tze wurden importiert.</p>";
						fclose($handle);
					}
				}
			}
		}
		/* evaluate after-post status */
		if($info) echo $this->create_message_box($info, FALSE);
		if($error) echo $this->create_message_box($error, TRUE);
		/* Output Settings Section */
		echo
		'
				<h3>Daten Import</h3>
				<p>An dieser Stelle kann die Datei "Streckmittelmeldungen.txt" zum Server &uuml;bertragen und in die Datenbank importiert werden.</p>
				<form method="post" enctype="multipart/form-data">
				<table class="form-table">
				<tbody>
				<tr valign="top">
				<th scope="row">Streckmittel Textdatei ausw&auml;hlen</th>
				<td><input id="dhv-s3m-filename" name="dhv-s3m-filename" type="file" size="60" /></td>
				</tr>
				</tbody>
				</table>
				<p>
				<input type="checkbox" checked id="dhv-s3m-csv-import-geocode" name="dhv-s3m-csv-import-geocode" />Geokodierung durchf&uuml;hren
				<input type="checkbox" id="dhv-s3m-csv-import-overwrite" name="dhv-s3m-csv-import-overwrite" />Vorhandene Daten l&ouml;schen
				</p>
				<p>
				<i>Achtung! Die Streckmittel Textdatei muss im Zeichensatz ISO-8859-15 (Deutsch + Euro) verfasst sein, damit Umlaute und Sonderzeichen korrekt interpretiert werden! Eine typische DHV Streckmittelmeldungen.txt ist bereits ISO-8859-15 kodiert.</i>
				</p>
				<p class="submit">
				<input class="button-primary" id="dhv-s3m-csv-import-submit" name="dhv-s3m-csv-import-submit" value="Hochladen &amp; Importieren" type="submit" />
				<input class="button-primary" id="dhv-s3m-csv-import-delete-db" name="dhv-s3m-csv-import-delete-db" value="Datenbank leeren" type="submit" />
				</p>
				</form>
				';
		$this->common_footer();
	}

	public function settings_section()
	{ /* I'm required by wordpress for no aparent reason except maybe saving an "isset" -.- */ }

	public function settings_page()
	{
		$this->common_header('Einstellungen');

		echo '<form method="post" action="options.php">';
		settings_fields('dhv-s3m-settings-group');
		do_settings_sections('dhv-s3m-settings-page');
		echo
		'
				<p class="submit">
				<input name="Submit" type="submit" class="button-primary" value="Einstellungen speichern" />
				</p>
				</form>
				';

		$this->common_footer();
	}
};

new DHV_S3M_Admin();

?>
